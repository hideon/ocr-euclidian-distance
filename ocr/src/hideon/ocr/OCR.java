package hideon.ocr;

import hideon.ocr.component.DrawingArea;
import hideon.ocr.component.GridArea;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class OCR extends JFrame {
	private static final long serialVersionUID = 1L;
	
	protected DrawingArea	drawingArea = null;
	protected GridArea		gridArea = null;
	
	protected JButton	drawSampleBtn = null;
	protected JButton	recognizeBtn  = null;
	protected JButton	resetBtn      = null;
	protected JButton	learnBtn      = null;
	
	protected Map<Character,int[]> definedCharacters = new HashMap<Character,int[]>();
	
	public OCR() {
		super("OCR");
		this.setContentPane(new JPanel());
		
		// Area para desenhar
		drawingArea = new DrawingArea();
		drawingArea.setPreferredSize(new Dimension(5*40,8*40));
		drawingArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.getContentPane().add(drawingArea);
		
		// Area do grid
		gridArea = new GridArea(5,8);
		gridArea.setPreferredSize(new Dimension(5*20,8*20));
		gridArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.getContentPane().add(gridArea);
		
		// Botao de desenhar no grid
		drawSampleBtn = new JButton("Draw Sample");
		drawSampleBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gridArea.convertLinesToData(drawingArea.lines, drawingArea.getWidth(), drawingArea.getHeight());
			}
		});
		this.getContentPane().add(drawSampleBtn);
		
		// Botao de reconhecimento
		recognizeBtn = new JButton("Recognize");
		recognizeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gridArea.convertLinesToData(drawingArea.lines, drawingArea.getWidth(), drawingArea.getHeight());
				recognize();
			}
		});
		this.getContentPane().add(recognizeBtn);
		
		// Botao de reset
		resetBtn = new JButton("Reset");
		resetBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				drawingArea.reset();
				gridArea.reset();
			}
		});
		this.getContentPane().add(resetBtn);
		
		// Botao para aprender novo caractere
		learnBtn = new JButton("Learn");
		learnBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String c = JOptionPane.showInputDialog("Insert character to learn: ");
				definedCharacters.put(c.charAt(0), gridArea.data);
			}
		});
		this.getContentPane().add(learnBtn);
		
		// Configuracao do frame
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(400, 440);
		this.setVisible(true);
	}
	
	public void recognize() {
		char bestChar = 0;
		double bestScore = 0;
		
		// Compara os dados do grid com cada caractere definido
		for(Map.Entry<Character, int[]> e: definedCharacters.entrySet()) {
			int[] dataG = gridArea.data;
			int[] dataC = e.getValue();
			
			// Distancia Euclidiana
			double sum = 0;
			float delta;
			for(int i = 0; i < dataG.length; ++i) {
				delta = dataC[i] - dataG[i];
				sum += delta*delta;
			}
			sum = Math.sqrt(sum);
			
			// Seleciona a menor pontuacao dentre todos
			if(sum < bestScore || bestChar == 0) {
				bestChar = e.getKey();
				bestScore = sum;
			}
		}
		
		// Mostra o caractere encontrado
		JOptionPane.showMessageDialog(null, "The char is: "+bestChar);
	}
	
	public static void main(String[] args) {
		OCR ocr = new OCR();
		
		// Define os caracteres
		ocr.definedCharacters.put('0', new int[]{-1, 1, 1, 1,-1, 1, 1,-1, 1, 1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1, 1, 1,-1,-1, 1,-1, 1, 1, 1,-1});
		ocr.definedCharacters.put('1', new int[]{ 1, 1, 1,-1,-1, 1, 1, 1,-1,-1, 1, 1, 1,-1,-1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1, 1,-1, 1, 1, 1, 1});
		ocr.definedCharacters.put('2', new int[]{ 1, 1, 1,-1,-1,-1,-1, 1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1, 1, 1, 1, 1,-1, 1,-1, 1, 1,-1, 1, 1, 1, 1, 1});
		ocr.definedCharacters.put('3', new int[]{ 1, 1, 1, 1,-1,-1,-1,-1, 1, 1,-1,-1,-1, 1, 1,-1,-1, 1, 1,-1,-1, 1, 1, 1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1, 1, 1, 1, 1, 1});
		ocr.definedCharacters.put('4', new int[]{ 1,-1,-1, 1,-1, 1,-1,-1, 1,-1, 1,-1,-1, 1,-1, 1, 1, 1, 1, 1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1});
		ocr.definedCharacters.put('5', new int[]{ 1, 1, 1, 1, 1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1, 1, 1, 1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1, 1, 1, 1, 1, 1});
		ocr.definedCharacters.put('6', new int[]{-1, 1, 1, 1,-1, 1, 1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1, 1, 1,-1, 1, 1, 1, 1, 1, 1, 1,-1,-1, 1, 1, 1,-1,-1, 1,-1, 1, 1, 1, 1});
		ocr.definedCharacters.put('7', new int[]{ 1, 1, 1, 1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1,-1,-1,-1});
		ocr.definedCharacters.put('8', new int[]{ 1, 1, 1, 1, 1, 1,-1,-1,-1, 1, 1,-1,-1,-1, 1, 1, 1, 1, 1, 1,-1, 1, 1, 1, 1, 1, 1,-1,-1, 1, 1,-1,-1,-1, 1, 1, 1, 1, 1, 1});
		ocr.definedCharacters.put('9', new int[]{ 1, 1, 1, 1, 1, 1, 1,-1,-1, 1, 1,-1,-1,-1, 1, 1, 1, 1, 1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1,-1,-1,-1,-1, 1});
	}
}
