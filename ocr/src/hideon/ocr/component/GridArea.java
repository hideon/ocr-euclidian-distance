package hideon.ocr.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JPanel;

public class GridArea extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public int[] data = null;
	public int gridWidth;
	public int gridHeight;
	
	public GridArea(int width, int height) {
		super();
		
		gridWidth = width;
		gridHeight = height;
		
		// Inicializa dados para -1
		data = new int[width*height];
		for(int i = 0; i < data.length; ++i) {
			data[i] = -1;
		}
	}
	
	public void reset() {
		// Reseta dados para -1
		for(int i = 0; i < data.length; ++i) {
			data[i] = -1;
		}
		
		// Redesenha os graficos
		this.repaint();
	}
	
	public void convertLinesToData(List<Line2D> lines, int width, int height) {
		// Escala
		float rW = getWidth() / (float)width;
		float rH = getHeight() / (float)height;
		
		if(lines.size() > 0) {
			// Reinicializa dados
			reset();
			
			// Retangulo (celula do grid) e Linha redimensionada
			Rectangle2D.Float r = null;
			Line2D l = null;
			
			// Incremento / dimensao da celula
			float wInc = getWidth() / (float)gridWidth;
			float hInc = getHeight() / (float)gridHeight;
			
			// Para cada linha
			for(Line2D line : lines) {
				l = new Line2D.Double(rW*line.getX1(), rH*line.getY1(), rW*line.getX2(), rH*line.getY2());
				
				// Verifica se existe intersecao entre a linha e a celula
				for(int j = 0; j < data.length; ++j) {
					int ii = j % gridWidth;
					int jj = j / gridWidth;
					
					r = new Rectangle2D.Float(wInc*ii, hInc*jj, wInc, hInc);
					
					// Define a celula como pintada
					if(l.intersects(r)) {
						data[j] = 1;
					}
				}
			}
			
			// Redesenha
			this.repaint();
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// Dimensoes da celula
		int wInc = getWidth() / gridWidth;
		int hInc = getHeight() / gridHeight;
		
		// Para cada celula
		for(int i = 0; i < data.length; ++i) {
			int ii = i % gridWidth;
			int jj = i / gridWidth;
			
			int d = data[i];
			
			// Pinta a celula
			g.setColor(Color.WHITE);
			if(d == 1)
				g.setColor(Color.BLACK);
			g.fillRect(wInc*ii, hInc*jj, wInc, hInc);
			
			g.setColor(Color.BLACK);
			g.drawRect(wInc*ii, hInc*jj, wInc, hInc);
		}
	}
}
