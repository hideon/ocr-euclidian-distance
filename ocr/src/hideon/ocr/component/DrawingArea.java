package hideon.ocr.component;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class DrawingArea extends JPanel implements MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 1L;
	
	public List<Line2D> lines = new ArrayList<Line2D>();
	private Point lastPoint = null;
	
	public DrawingArea() {
		super();
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	
	public void reset() {
		lines.clear();
		this.repaint();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// Fundo branco
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// Pinta as linhas
		g.setColor(Color.BLACK);
		if(lines.size() > 0) {
			for(Line2D l : lines) {
				g.drawLine((int)l.getX1(), (int)l.getY1(), (int)l.getX2(), (int)l.getY2());
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) { }

	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }

	@Override
	public void mousePressed(MouseEvent e) { }

	@Override
	public void mouseReleased(MouseEvent e) {
		lastPoint = null;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (e.getModifiersEx() != MouseEvent.BUTTON1_DOWN_MASK)
		    return;
		
		// Adiciona ponto
		Point p = e.getPoint();
		if(lastPoint != p) {
			if(lastPoint == null)
				lastPoint = p;
			Line2D l = new Line2D.Double(lastPoint.x, lastPoint.y, p.x, p.y);
			lines.add(l);
			this.repaint();
		}
		lastPoint = p;
	}

	@Override
	public void mouseMoved(MouseEvent e) { }
}
